import "./App.css";
import { Col, Row } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";
import { Balance } from "./components/Balance";
import { BalanceChange } from "./components/BalanceChange";
import { BankNotes } from "./components/BankNotes";
import { Coin } from "./components/Coin";
import { PickUp } from "./components/PickUp";
import { Items } from "./components/Items";

function App() {
  const [money, setmoney] = useState(0);
  const [change, setchange] = useState([]);
  const [balance, setbalance] = useState([]);
  const [receive, setreceive] = useState({});
  const [items, setitems] = useState([]);

  const getItems = async () => {
    let { data, status } = await axios.get(`http://localhost:3000/api/items`);

    if (status === 200) setitems(data);
  };

  const getChange = async () => {
    let { data, status } = await axios.get(`http://localhost:3000/api/change`);

    if (status === 200) setchange(data);
  };

  useEffect(() => {
    getItems();
    getChange();
  }, []);

  return (
    <div className="App">
      <br />
      <div
        className="vending-machine-container"
        style={{
          backgroundImage:
            "url('https://i.pinimg.com/originals/01/9e/ff/019effebf4db2ca7d99231b41c9c241b.jpg')",
        }}
      >
        <Row>
          <Col xs={12} md={7} lg={8}>
            <Items
              items={items}
              money={money}
              setmoney={setmoney}
              change={change}
              receive={receive}
              setreceive={setreceive}
              setbalance={setbalance}
              getItems={getItems}
              getChange={getChange}
            />

            <div className="border-black pd-10 mt-20">
              <PickUp
                receive={receive}
                setmoney={setmoney}
                setreceive={setreceive}
                setbalance={setbalance}
              />
            </div>
          </Col>

          <Col xs={12} md={5} lg={4}>
            <div className="border-black">
              <Coin
                money={money}
                setmoney={setmoney}
                isActive={Object.keys(receive).length === 0 ? false : true}
              />

              <BankNotes
                isActive={Object.keys(receive).length === 0 ? false : true}
                money={money}
                setmoney={setmoney}
              />

              <div className="deposit-container">
                <h5>Your Money: {money.toLocaleString()} THB</h5>
              </div>
            </div>

            <Balance
              balance={balance}
              setbalance={setbalance}
              setreceive={setreceive}
              setmoney={setmoney}
            />

            <br />
            <div className="center-item">
              <BalanceChange change={change} />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default App;
