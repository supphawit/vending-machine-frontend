import React from "react";

export const BalanceChange = ({ change }) => {
  return (
    <table border="1" className="center-item bg-white">
      <tbody>
        <tr>
          <td colSpan={8}> Balance Change</td>
        </tr>

        <tr>
          {change.map((value, index) => (
            <td key={index} className="pd-5">
              {value.amount.toLocaleString()}
            </td>
          ))}
        </tr>

        <tr>
          {change.map((value, index) => (
            <td key={index} className="pd-5">
              {value.qty}
            </td>
          ))}
        </tr>
      </tbody>
    </table>
  );
};
