import React from "react";

export const PickUp = ({ receive, setmoney, setreceive, setbalance }) => {
  return (
    <div
      className="text-center border-black pd-10 bg-white"
      style={{ height: 80 }}
    >
      {Object.keys(receive).length !== 0 && (
        <img
          alt=""
          src={`./images/${receive.name}.png`}
          width={receive && receive.img_size / 2}
          style={{ margin: 5, cursor: "pointer" }}
          onClick={() => {
            setmoney(0);
            setreceive({});
            setbalance([]);
          }}
        />
      )}
    </div>
  );
};
