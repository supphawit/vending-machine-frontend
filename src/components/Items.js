import axios from "axios";
import React from "react";
import { Button } from "react-bootstrap";

export const Items = ({
  items,
  money,
  setmoney,
  change,
  receive,
  setreceive,
  setbalance,
  getItems,
  getChange,
}) => {
  
  const buy_item = async (item) => {
    let _tmp = return_change(money - item.price);
    let { status } = await axios.post(`http://localhost:3000/api/buy_item`, {
      item_id: item.id,
      return_change: _tmp,
    });

    if (status === 200) {
      setmoney(money - item.price);
      setreceive(item);
      setbalance(_tmp);
      getChange();
      getItems();
    }
  };

  const return_change = (number) => {
    let tmp = number;
    let balance_change = [];
    while (tmp > 0) {
      if (tmp >= 1000) {
        balance_change.push({ amount: 1000, qty: parseInt(tmp / 1000) });
        tmp %= 1000;
      } else if (tmp >= 500) {
        balance_change.push({ amount: 500, qty: parseInt(tmp / 500) });
        tmp %= 500;
      } else if (tmp >= 100) {
        balance_change.push({ amount: 100, qty: parseInt(tmp / 100) });
        tmp %= 100;
      } else if (tmp >= 50) {
        balance_change.push({ amount: 50, qty: parseInt(tmp / 50) });
        tmp %= 50;
      } else if (tmp >= 20) {
        balance_change.push({ amount: 20, qty: parseInt(tmp / 20) });
        tmp %= 20;
      } else if (tmp >= 10) {
        balance_change.push({ amount: 10, qty: parseInt(tmp / 10) });
        tmp %= 10;
      } else if (tmp >= 5) {
        balance_change.push({ amount: 5, qty: parseInt(tmp / 5) });
        tmp %= 5;
      } else if (tmp >= 1) {
        balance_change.push({ amount: 1, qty: parseInt(tmp / 1) });
        tmp %= 1;
      }
    }
    return balance_change;
  };

  const enough_change = (price) => {
    let result = true;
    let arr_balance = return_change(money - price);
    arr_balance.forEach((b) => {
      let x = change.find((e) => e.amount === b.amount);
      if (x.qty < b.qty) result = false;
    });

    return result;
  };

  const isActive = (item) => {
    let result = false;
    if (
      money >= item.price &&
      enough_change(item.price) &&
      item.qty > 0 &&
      Object.keys(receive).length === 0
    ) {
      result = true;
    }

    return result;
  };
  return (
    <div className="menu-container bg-white">
      {items.map((item, index) => (
        <Button
          key={index}
          className="menu-item"
          variant={isActive(item) ? "outline-success" : "outline-danger"}
          onClick={(e) => {
            if (isActive(item)) {
              buy_item(item);
            }
          }}
          disabled={!isActive(item)}
        >
          <span
            className={`${isActive(item) ? "dot-active" : "dot-inactive"}`}
          ></span>
          <br />
          <img alt="" src={`./images/${item.name}.png`} width={item.img_size} />
          <br />
          {item.price} THB
          <br />
          {item.qty > 0 ? `Qty: ${item.qty}` : `Out of Stock`}
        </Button>
      ))}
    </div>
  );
};
