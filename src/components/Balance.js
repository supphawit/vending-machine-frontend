import React from "react";
import { Button } from "react-bootstrap";

export const Balance = ({ balance, setbalance, setreceive, setmoney }) => {
  return (
    <div
      className="center-item border-black pd-10 mt-20 "
      style={{
        height: 180,
      }}
    >
      <div
        className="center-item border-black pd-10 bg-white "
        style={{
          height: 160,
        }}
      >
        <h5>Change</h5>

        <div className="deposit-container">
          {balance.map((value, index) => {
            if (value.amount > 10) {
              return (
                <Button
                  key={index}
                  className={`btn-receive-banknotes  banknotes-${value.amount}`}
                  variant="info"
                  onClick={() => {
                    setmoney(0);
                    setreceive({});
                    setbalance([]);
                  }}
                >
                  <span className="btn-receive-text">
                    {value.amount}x{value.qty}
                  </span>
                </Button>
              );
            } else {
              return (
                <Button
                  key={index}
                  className="btn-receive-number"
                  variant="info"
                  onClick={() => {
                    setmoney(0);
                    setreceive({});
                    setbalance([]);
                  }}
                >
                  <span className="btn-receive-text">
                    {value.amount}x{value.qty}
                  </span>
                </Button>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
};
