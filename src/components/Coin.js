import React from "react";
import { Button } from "react-bootstrap";

export const Coin = ({ money, setmoney, isActive }) => {
  return (
    <div className="deposit-container">
      {[1, 5, 10].map((val, index) => (
        <Button
          key={index}
          className="btn-number number-space"
          variant="info"
          onClick={() => {
            setmoney(money + val);
          }}
          disabled={isActive}
        >
          <span className="btn-text">{val}</span>
        </Button>
      ))}
    </div>
  );
};
