import React from "react";
import { Button } from "react-bootstrap";

export const BankNotes = ({ money, setmoney, isActive }) => {
  return (
    <>
      {[[20, 50], [100, 500], [1000]].map((value, index) => {
        let data = [];
        value.map((val, i) => {
          data.push(
            <Button
              key={index + "" + i}
              className={`btn-banknotes number-space banknotes-${val}`}
              variant="info"
              onClick={() => {
                setmoney(money + val);
              }}
              disabled={isActive}
            >
              <span className="btn-text">{val.toLocaleString()}</span>
            </Button>
          );
          return null;
        });
        return (
          <div key={index} className="deposit-container">
            {data}
          </div>
        );
      })}
    </>
  );
};
